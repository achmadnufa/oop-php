<?php
    require_once("animal.php");
    require_once("Ape.php");
    require_once("Frog.php");

    $animal = New Animal("shaun");
    echo "Name      : ".$animal->name."<br>";
    echo "Legs      : ".$animal->legs."<br>";
    echo "Cold blooded : ".$animal->cold_blooded."<br>";
    echo "<br>";
    
    $kodok = new Frog("buduk");
    echo "Name      : ".$kodok->name."<br>";
    echo "Legs      : ".$kodok->legs."<br>";
    echo "Cold blooded : ".$kodok->cold_blooded."<br>";
    $kodok->jump() ; // "hop hop"
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name      : ".$sungokong->name."<br>";
    echo "Legs      : ".$sungokong->legs."<br>";
    echo "Cold blooded : ".$sungokong->cold_blooded."<br>";
    $sungokong->yell();// "Auooo"
    echo "<br><br>";

    
    



?>
